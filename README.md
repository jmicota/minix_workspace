Damian Werpachowski:

Przykładowy Workflow do modyfikacji MINIXa

Zawiera skrypty do stawiania minixa, uruchamiania, generowania diffa, uploadowania zmian i kompilowania minixa.

Zawiera także podzbiór źródeł podlinkowany przez Adama B.boRODO (o te: https://www.mimuw.edu.pl/~mb346851/SO2021/minix_source.tar.xz) oraz testy Andrzeja S.boRODO.

W folderze OriginalSources znajduje się niezmieniony podzbiór źródeł, natomiast praca nad kodem odbywa się w folderze Sources. Jest tam repozytorium Gita, w którym master wskazuje na oryginalną wersję oraz dodatkowo są branche Task3, Task4, Task5, Task6, czyli oddzielne na każde zadanie. Od razu jest checkoutnięte Task3.

Instrukcja przygotowania workflow:
1. Do folderu BASE należy wrzucić minix.img ściągnięte z Moodle.

2. Zmodyfikować u siebie na komputerze plik ~/.ssh/config poprzez dodanie na koniec:
Host minix
        HostName localhost
        Port 10022
        User root

3. Skrypty znajdują się w folderze Scripts i z niego należy je uruchamiać. Trzeba najpierw poprawić numery indeksów (tj zamienić dw407214 na swój indeks) wewnątrz plików genDiff.sh/patchMinix.sh/uploadPatchScriptTests.sh

Przykładowy workflow:
1. Pracujemy na plikach w Sources/usr. Jak już zmienimy pliki to przechodzimy do folderu Scripts.
2. ./newMinix.sh stworzy nowego MINIXa (będzie umieszczony w folderze RUNNING).
3. ./runMinix.sh uruchomi MINIXa.
4. W drugim terminalu robimy ./genDiff.sh (patch pojawi się w SO/MINIX), a następnie (ale dopiero jak w pierwszym terminalu już się uruchomi MINIX i się na niego zalogujemy) ./uploadPatchScriptTests.sh
5. Pliki już są na MINIXie, można wejść i przejść do folderu /.
6. Stamtąd uruchamiamy ./patchMinix.sh
7. Po restarcie wchodzimy do /SOTesty i uruchamiamy ./start.sh